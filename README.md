# CloudControl

## Steps to run the app

1. Clone repo to your local
2. Ensure you have docker installed
3. In your terminal, cd to the project, and run "docker-compose up" (this will build and run the client and server docker containers)
4. The app will be available at localhost:4200

## App architecture
-Client = Angular 7 (with angular materia), Server = Node.js/express.
-Both client/server have a docker configuration which can be run individually, or together using docker-compose.
-The clientside docker uses an nginx webserver image, serverside uses a nodejs alpine linux image.

##Note
-Removed --prod flag from angular build due to encountering a last-minute error "Function calls are not supported in decorators but 'AppRoute' was called in 'AppRoutes' 'AppRoutes' calls 'AppRoute'" which appears to be caused by an Angular AOT bug (similar issue here https://github.com/angular/angular/issues/23609) - with more time I could investigate this.

## Issues
1. On the create server page, the CPU number input validations are incomplete, and the privateIpv4 validation is not added.


#More details on commands:

##To build an image only if it doesn't exist, and run containers
docker-compose up

##To force build of containers
docker-compose up --build

##To shut down everything
docker-compose down

##To shutdown and remove all images
docker-compose down --rmi all