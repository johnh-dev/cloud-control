import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OrganizationsComponent } from './components/organizations/organizations.component';
import { ServerEditComponent } from './components/server-edit/server-edit.component';
import { ServerDeleteComponent, ServerDeleteConfirmDialog } from './components/server-delete/server-delete.component';
import { ServerCreateComponent } from './components/server-create/server-create.component';
import { OrganizationDashboardComponent } from './components/organization-dashboard/organization-dashboard.component';
import { ServerDashboardComponent } from './components/server-dashboard/server-dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableComponent } from './components/table/table.component';
import { DemoMaterialModule } from './material-module';
import { OrganizationsService } from './services/organizations/organizations.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NavigationComponent } from './components/navigation/navigation.component';
import { SidenavService } from './services/sidenav.service';
import { RouteService } from './services/route.service';
import { HelpComponent } from './components/help/help.component';
import { ServerService } from './services/servers/server.service';
import { StepperComponent } from './components/stepper/stepper.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProgressSpinnerService } from './services/progress-spinner.service';
import { ProgressSpinnerInterceptor } from './interceptors/progress-spinner.interceptor';
import { ProgressSpinnerComponent } from './components/progress-spinner/progress-spinner.component';
import { ErrorHandlerInterceptor } from './interceptors/error-handler.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    OrganizationsComponent,
    OrganizationDashboardComponent,
    ServerCreateComponent,
    ServerDashboardComponent,
    ServerDeleteComponent,
    ServerEditComponent,
    TableComponent,
    NavigationComponent,
    HelpComponent,
    StepperComponent,
    ProgressSpinnerComponent,
    ServerDeleteConfirmDialog
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    OrganizationsService,
    SidenavService,
    RouteService,
    ServerService,
    ProgressSpinnerService,
    { provide: HTTP_INTERCEPTORS, useClass: ProgressSpinnerInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorHandlerInterceptor, multi: true }
  ],
  entryComponents: [ServerDeleteConfirmDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
