import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganizationsComponent } from './components/organizations/organizations.component';
import { ServerCreateComponent } from './components/server-create/server-create.component';
import { ServerDeleteComponent } from './components/server-delete/server-delete.component';
import { ServerEditComponent } from './components/server-edit/server-edit.component';
import { OrganizationDashboardComponent } from './components/organization-dashboard/organization-dashboard.component';
import { ServerDashboardComponent } from './components/server-dashboard/server-dashboard.component';
import { AppRoutes } from './classes/routes';
import { HelpComponent } from './components/help/help.component';

const routes: Routes = [
  {path: AppRoutes.Help.path, component: HelpComponent},
  {path: AppRoutes.Orgs.path, component: OrganizationsComponent},
  {path: AppRoutes.OrgDashboard.path, component: OrganizationDashboardComponent},
  {path: AppRoutes.ServerCreate.path, component: ServerCreateComponent},
  {path: AppRoutes.ServerDashboard.path, component: ServerDashboardComponent},
  {path: AppRoutes.ServerDelete.path, component: ServerDeleteComponent},
  {path: AppRoutes.ServerEdit.path, component: ServerEditComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
