import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { ProgressSpinnerService } from '../../services/progress-spinner.service';

@Component({
  selector: 'app-progress-spinner',
  templateUrl: './progress-spinner.component.html',
  styleUrls: ['./progress-spinner.component.scss']
})
export class ProgressSpinnerComponent {
  color = 'primary';
  mode = 'indeterminate';
  isLoading: Subject<boolean> = this.progressSpinnerService.isLoading;
  
  constructor(private progressSpinnerService: ProgressSpinnerService){}
}
