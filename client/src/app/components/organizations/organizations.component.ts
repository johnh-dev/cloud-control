import { Component, OnInit } from '@angular/core';
import { OrganizationsService } from 'src/app/services/organizations/organizations.service';
import { TableType } from 'src/app/classes/table-type';
import { OrganizationsTableData } from 'src/app/classes/organizations-table-data';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.scss']
})
export class OrganizationsComponent implements OnInit {

  tableType = TableType;

  displayData: OrganizationsTableData[];
  displayColumns: String[];

  constructor(
    public organizationsService: OrganizationsService
  ) { }

  ngOnInit() {
    this.organizationsService.getOrganizations()
    .subscribe(organizationsResponse => {
      const organizations = organizationsResponse.organizations;
      this.displayData = [];
      organizations.forEach(org => {
        this.displayData.push(
          {
            id: org.id,
            name: org.name,
            serverCount: org.server.length
          }
        );
      });
      this.displayColumns = ['name', 'serverCount'];
    });
  }

}
