import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OrganizationsComponent } from './organizations.component';
import { TableComponent } from '../table/table.component';
import { DemoMaterialModule } from 'src/app/material-module';
import { OrganizationsService } from 'src/app/services/organizations/organizations.service';
import { OrganizationsMockService } from 'src/app/services/organizations/organizations-mock.service';

describe('OrganizationsComponent', () => {
  let component: OrganizationsComponent;
  let fixture: ComponentFixture<OrganizationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        DemoMaterialModule
      ],
      declarations: [ 
        OrganizationsComponent,
        TableComponent
      ],
      providers: [
        {provide: OrganizationsService, useClass: OrganizationsMockService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
