import { Component, OnInit } from '@angular/core';
import { ServerService } from 'src/app/services/servers/server.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

  constructor(
    private serverService: ServerService
  ) { }

  ngOnInit() {
  }

  triggerError() {
    this.serverService.getServer('nonExistentOrg', 'nonExistentServer').subscribe();
  }

}
