import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerCreateComponent } from './server-create.component';
import { ServerMockService } from 'src/app/services/servers/server-mock.service';
import { ServerService } from 'src/app/services/servers/server.service';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { RouteService } from 'src/app/services/route.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { StepperComponent } from '../stepper/stepper.component';
import { FormBuilder, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DemoMaterialModule } from 'src/app/material-module';
import { RouterTestingModule } from '@angular/router/testing';
import { SidenavService } from 'src/app/services/sidenav.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ServerCreateComponent', () => {
  let component: ServerCreateComponent;
  let fixture: ComponentFixture<ServerCreateComponent>;

  const fakeActivatedRoute = {
    params: of({organization: 'orgidtest-122121123'})
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        ServerCreateComponent,
        StepperComponent
      ],
      imports: [
        DemoMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      providers: [
        RouteService,
        SnackbarService,
        SidenavService,
        FormBuilder,
        {provide: ServerService, useValue: ServerMockService},
        {provide: ActivatedRoute, useValue: fakeActivatedRoute}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create a server', () => {
    expect(component).toBeTruthy();
  });
});
