import { Component, OnInit } from '@angular/core';
import { CreateServerDTO } from 'src/app/classes/create-server-dto';
import { ServerService } from 'src/app/services/servers/server.service';
import { ActivatedRoute } from '@angular/router';
import { RouteService } from 'src/app/services/route.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { AppRoutes } from 'src/app/classes/routes';
import { ServerUpdateType } from 'src/app/classes/server-update-type';


@Component({
  selector: 'app-server-create',
  templateUrl: './server-create.component.html',
  styleUrls: ['./server-create.component.scss']
})
export class ServerCreateComponent implements OnInit {

  type: ServerUpdateType = ServerUpdateType.Create;
  serverData: CreateServerDTO;
  orgId: string;

  constructor(
    public serverService: ServerService,
    private route: ActivatedRoute,
    private routeService: RouteService,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.orgId = params['organization'];
    });
  }

  handleCreateServerSubmit(serverDetails: CreateServerDTO) {
    const serverID = this.serverService.generateUUID();
    const networkID = this.serverService.generateUUID();
    const networkVlanID = this.serverService.generateUUID();
    const createTime: Date = new Date();
    const serverPostData: any = serverDetails;
    serverPostData.id = serverID;
    serverPostData.network.id = networkID;
    serverPostData.network.vlanId = networkVlanID;
    serverPostData.createTime = createTime;
    serverPostData.state = 'NORMAL';
    this.serverService.createServer(this.orgId, serverPostData).subscribe(resp => {
    this.snackbarService.show('Successfully Created Server: ' + serverDetails.name);
      this.routeService.setRoute(AppRoutes.OrgDashboard, null, 1);
    });
  }
}
