import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationComponent } from './navigation.component';
import { RouteService } from 'src/app/services/route.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import { DemoMaterialModule } from 'src/app/material-module';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        DemoMaterialModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [ NavigationComponent ],
      providers: [
        SidenavService,
        RouteService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
