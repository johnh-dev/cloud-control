import { Component, OnInit } from '@angular/core';
import { SidenavService } from 'src/app/services/sidenav.service';
import { RouteService } from 'src/app/services/route.service';
import { AppRoutes } from 'src/app/classes/routes';
import { Location } from '@angular/common';
import { SidenavItem } from 'src/app/classes/sidenav-item';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(
    public sidenavService: SidenavService,
    private routeService: RouteService
  ) { }

  ngOnInit() {
    this.routeService.setRoute(AppRoutes.Orgs);
  }

  execute(item: SidenavItem) {
    if(item.func) {
      item.execute();
    } else if (item.skipBack !== null && item.skipBack !== undefined) {
      this.routeService.setRoute(item.route, null, item.skipBack)
    } else {
      this.routeService.setRoute(item.route);
    }
  }
}
