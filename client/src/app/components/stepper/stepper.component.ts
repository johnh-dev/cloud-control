import {Component, OnInit, EventEmitter, Output, Input, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { CreateServerDTO } from 'src/app/classes/create-server-dto';
import { Server } from 'src/app/classes/server';
import { ServerUpdateType } from 'src/app/classes/server-update-type';
import { BehaviorSubject } from 'rxjs';

/**
 * @title Stepper with editable steps
 */
@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {
  serverUpdateType = ServerUpdateType;

  basicDetailsFormGroup: FormGroup;
  cpuAndMemoryFormGroup: FormGroup;
  networkFormGroup: FormGroup;
  launchFormGroup: FormGroup;
  isEditable = true;

  cpuSpeeds = [ "ECONOMY", "STANDARD", "HIGHPERFORMANCE" ];
  states = [ "NORMAL", "FAILED", "MAINTENANCE" ];

  @Input() type: ServerUpdateType;
  @Input() editServerDTO: Server;

  @Output() createServer: EventEmitter<CreateServerDTO> = new EventEmitter();
  @Output() editServer: EventEmitter<Server> = new EventEmitter();

  isSetupComplete: boolean;
  //TODO fix the CPU number input validations which are still allowing numbers which are too small or large

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.setupForm();
  }

  /**
   * Sets up either the create/edit server data and loads it into the stepper using _formBuilder
   */
  setupForm() {
    let name;
    let description;
    let cpuSpeed;
    let cpuCount;
    let coresPerSocket;
    let memory;
    let privateIpv4;
    let vlanName;
    let deployed;
    let started;
    if(this.type === ServerUpdateType.Create) {
      name = '';
      description = '';
      cpuSpeed = 'STANDARD';
      cpuCount = 4;
      coresPerSocket = 4;
      memory = 4;
      privateIpv4 = '0.0.0.0';
      vlanName = '';
      deployed = false;
      started = false;
    } else if (this.type === ServerUpdateType.Edit){
      name = this.editServerDTO.name;
      description = this.editServerDTO.description;
      cpuSpeed = this.editServerDTO.cpu.speed;
      cpuCount = this.editServerDTO.cpu.count;
      coresPerSocket = this.editServerDTO.cpu.coresPerSocket;
      memory = this.editServerDTO.memoryGb;
      privateIpv4 = this.editServerDTO.network.privateIpv4;
      vlanName = this.editServerDTO.network.vlanName;
      deployed = this.editServerDTO.deployed;
      started = this.editServerDTO.started;
    }
    this.basicDetailsFormGroup = this._formBuilder.group({
      nameCtrl: [name, Validators.required],
      descriptionCtrl: [description, Validators.required],
    });
    this.cpuAndMemoryFormGroup = this._formBuilder.group({
      cpuSpeedCtrl: [cpuSpeed, Validators.required],
      cpuCountCtrl: [cpuCount, Validators.required],
      coresPerSocket: [coresPerSocket, [Validators.required, Validators.pattern(new RegExp(/^[0-9]*[02468]$/))]], // regex 'even-number only' validator.. need to make it restrict nubmers outside 1&64
      memoryCtrl: [memory, Validators.required]
    });
    this.networkFormGroup = this._formBuilder.group({
      privateIpv4Ctrl: [privateIpv4, Validators.required],
      vlanNameCtrl: [vlanName, Validators.required],
    });
    this.launchFormGroup = this._formBuilder.group({
      deployedCtrl: [{value: deployed, disabled: false}, Validators.required],
      startedCtrl: [{value: started, disabled: false}, Validators.required]
    });
    this.isSetupComplete = true;
  }

  /**
   * Emits either a createServer/editServer event containing the data to be sent to server
   */
  submitServer() {
    const name = this.basicDetailsFormGroup.controls.nameCtrl.value;
    const description = this.basicDetailsFormGroup.controls.descriptionCtrl.value;
    const cpuSpeed = this.cpuAndMemoryFormGroup.controls.cpuSpeedCtrl.value;
    const cpuCount = this.cpuAndMemoryFormGroup.controls.cpuCountCtrl.value;
    const coresPerSocket = this.cpuAndMemoryFormGroup.controls.coresPerSocket.value;
    const memory = this.cpuAndMemoryFormGroup.controls.memoryCtrl.value;
    const privateIpv4 = this.networkFormGroup.controls.privateIpv4Ctrl.value;
    const vlanName = this.networkFormGroup.controls.vlanNameCtrl.value;
    const deployed = this.launchFormGroup.controls.deployedCtrl.value;
    const started = this.launchFormGroup.controls.startedCtrl.value;
    if(this.type === ServerUpdateType.Create) {
      const serverDTO : CreateServerDTO = {
        name: name,
        description: description,
        cpu: {
          count: cpuCount,
          speed: cpuSpeed,
          coresPerSocket: coresPerSocket
        },
        memoryGb: memory,
        network: {
          privateIpv4: privateIpv4,
          vlanName: vlanName,
        },
        deployed: deployed,
        started: started
      };
      this.createServer.emit(serverDTO);
    } else if (this.type === ServerUpdateType.Edit){
      const id = this.editServerDTO.id;
      const networkId = this.editServerDTO.network.id;
      const vlanId = this.editServerDTO.network.vlanId;
      const createTime = this.editServerDTO.createTime;
      const state = this.editServerDTO.state;
      const serverDTO : Server = {
        id: id,
        name: name,
        description: description,
        cpu: {
          count: cpuCount,
          speed: cpuSpeed,
          coresPerSocket: coresPerSocket
        },
        memoryGb: memory,
        network: {
          id: networkId,
          privateIpv4: privateIpv4,
          vlanName: vlanName,
          vlanId: vlanId
        },
        createTime: createTime,
        deployed: deployed,
        started: started,
        state: state
      };
      this.editServer.emit(serverDTO);
    }
  }
  
}