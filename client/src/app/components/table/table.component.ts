import {Component, OnInit, ViewChild, Input} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { TableType } from 'src/app/classes/table-type';
import { RouteService } from 'src/app/services/route.service';
import { AppRoutes } from 'src/app/classes/routes';
import { SidenavService } from 'src/app/services/sidenav.service';

/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
  selector: 'app-table',
  styleUrls: ['table.component.scss'],
  templateUrl: 'table.component.html',
})
export class TableComponent implements OnInit {
  
  tableType = TableType;
  
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = null;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Input() tableData: any;
  @Input() displayColumns: string[] = null;
  @Input() fullscreenColumns: string[] = null;
  @Input() type: TableType;

  constructor(
    private routeService: RouteService,
    private sidenavService: SidenavService
  ) {}

  ngOnInit() {
    this.setDisplayedColumns();
    this.dataSource = new MatTableDataSource(this.tableData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  goToRoute(id: string) {
    switch (this.type) {
      case TableType.Orgs:
        console.log('goToRoute passing id: ' + id);
        this.routeService.setRoute(AppRoutes.OrgDashboard, id);
        break;
      case TableType.Servers:
        this.routeService.setRoute(AppRoutes.ServerDashboard, id);
        break;
    }
  }

  setDisplayedColumns() {
    if(this.fullscreenColumns === null){
      this.displayedColumns = this.displayColumns;
    } else {
      this.sidenavService.sidenavVisibilityChange.subscribe(isOpened => {
        if(isOpened) {
          this.displayedColumns = this.displayColumns;
        } else {
          this.displayedColumns = this.fullscreenColumns;
        }
      });
    }
  }
}