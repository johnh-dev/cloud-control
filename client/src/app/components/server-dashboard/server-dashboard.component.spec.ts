import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerDashboardComponent } from './server-dashboard.component';
import { ServerService } from 'src/app/services/servers/server.service';
import { ServerMockService } from 'src/app/services/servers/server-mock.service';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { DemoMaterialModule } from 'src/app/material-module';

describe('ServerDashboardComponent', () => {
  let component: ServerDashboardComponent;
  let fixture: ComponentFixture<ServerDashboardComponent>;

  const fakeActivatedRoute = {
    params: of({organization: 'testOrgId1', server: 'testServerId1'})
  };
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerDashboardComponent ],
      imports: [
        DemoMaterialModule
      ],
      providers: [
        {provide: ServerService, useValue: ServerMockService},
        {provide: ActivatedRoute, useValue: fakeActivatedRoute}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
