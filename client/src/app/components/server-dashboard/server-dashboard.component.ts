import { Component, OnInit } from '@angular/core';
import { Server } from 'src/app/classes/server';
import { ActivatedRoute } from '@angular/router';
import { ServerService } from 'src/app/services/servers/server.service';

@Component({
  selector: 'app-server-dashboard',
  templateUrl: './server-dashboard.component.html',
  styleUrls: ['./server-dashboard.component.scss']
})
export class ServerDashboardComponent implements OnInit {

  server: Server;

  constructor(
    public serverService: ServerService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const orgId = params['organization'];
      const serverId = params['server'];

      this.serverService.getServer(orgId, serverId)
      .subscribe(server => {
        this.server = server;
      });
    });
  }

}
