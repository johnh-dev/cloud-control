import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerEditComponent } from './server-edit.component';
import { StepperComponent } from '../stepper/stepper.component';
import { DemoMaterialModule } from 'src/app/material-module';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteService } from 'src/app/services/route.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import { ServerMockService } from 'src/app/services/servers/server-mock.service';
import { ActivatedRoute } from '@angular/router';
import { ServerService } from 'src/app/services/servers/server.service';
import { of } from 'rxjs';

describe('ServerEditComponent', () => {
  let component: ServerEditComponent;
  let fixture: ComponentFixture<ServerEditComponent>;

  const fakeActivatedRoute = {
    params: of({organization: 'testOrgId1', server: 'testServerId1'})
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        ServerEditComponent,
        StepperComponent
      ],
      imports: [
        DemoMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      providers: [
        RouteService,
        SnackbarService,
        SidenavService,
        FormBuilder,
        {provide: ServerService, useValue: ServerMockService},
        {provide: ActivatedRoute, useValue: fakeActivatedRoute}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
