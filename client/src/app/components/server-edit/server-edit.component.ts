import { Component, OnInit } from '@angular/core';
import { ServerService } from 'src/app/services/servers/server.service';
import { Server } from 'src/app/classes/server';
import { ActivatedRoute } from '@angular/router';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { RouteService } from 'src/app/services/route.service';
import { AppRoutes } from 'src/app/classes/routes';
import { ServerUpdateType } from 'src/app/classes/server-update-type';

@Component({
  selector: 'app-server-edit',
  templateUrl: './server-edit.component.html',
  styleUrls: ['./server-edit.component.scss']
})
export class ServerEditComponent implements OnInit {

  type: ServerUpdateType = ServerUpdateType.Edit;
  server: Server;
  orgId: string;

  constructor(
    private serverService: ServerService,
    private route: ActivatedRoute,
    private snackbarService: SnackbarService,
    private routeService: RouteService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.orgId = params['organization'];
      const serverId = params['server'];

      this.serverService.getServer(this.orgId, serverId)
      .subscribe(server => {
        this.server = server;
      });
    });
  }

  handleEditServerSubmit(server: Server) {
    this.serverService.editServer(this.orgId, server).subscribe(resp => {
    this.snackbarService.show('Successfully Updated Server: ' + server.name);
      this.routeService.setRoute(AppRoutes.ServerDashboard, null, 1);
    });
  }

}
