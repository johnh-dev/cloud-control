import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerDeleteComponent } from './server-delete.component';
import { DemoMaterialModule } from 'src/app/material-module';
import { ServerMockService } from 'src/app/services/servers/server-mock.service';
import { ServerService } from 'src/app/services/servers/server.service';
import { RouteService } from 'src/app/services/route.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

describe('ServerDeleteComponent', () => {
  let component: ServerDeleteComponent;
  let fixture: ComponentFixture<ServerDeleteComponent>;

  const fakeActivatedRoute = {
    params: of({organization: 'testOrgId1', server: 'testServerId1'})
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerDeleteComponent ],
      imports: [
        DemoMaterialModule
      ],
      providers: [
        {provide: ActivatedRoute, useValue: fakeActivatedRoute}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
