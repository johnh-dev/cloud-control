import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ServerService } from 'src/app/services/servers/server.service';
import { RouteService } from 'src/app/services/route.service';
import { AppRoutes } from 'src/app/classes/routes';
import { SnackbarService } from 'src/app/services/snackbar.service';

export interface DialogData {
  orgId: string;
  serverId: string;
}

@Component({
  selector: 'app-server-delete',
  templateUrl: './server-delete.component.html',
  styleUrls: ['./server-delete.component.scss']
})
export class ServerDeleteComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) {}

  openDialog(): void {
    this.route.params.subscribe(params => {
      const serverId = params['server'];
      const orgId = params['organization'];
      const dialogRef = this.dialog.open(ServerDeleteConfirmDialog, {
        width: '500px',
        data: {serverId: serverId, orgId: orgId}
      });
    });
  }

  ngOnInit() {
  }

}

@Component({
  selector: 'app-server-delete-confirm-dialog',
  templateUrl: './server-delete-confirm-dialog.component.html',
})
export class ServerDeleteConfirmDialog {

  constructor(
    public dialogRef: MatDialogRef<ServerDeleteConfirmDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private serverService: ServerService,
    private routeService: RouteService,
    private snackbarService: SnackbarService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  deleteServer() {
    this.serverService.deleteServer(this.data.orgId, this.data.serverId)
    .subscribe(response => {
      this.dialogRef.close();
      this.snackbarService.show('Server Deletion Success');
      this.routeService.setRoute(AppRoutes.OrgDashboard, null, 2);
    });
  }

}
