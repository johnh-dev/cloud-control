import { Component, OnInit } from '@angular/core';
import { TableType } from 'src/app/classes/table-type';
import { ServerService } from 'src/app/services/servers/server.service';
import { ActivatedRoute } from '@angular/router';
import { ServersTableData } from 'src/app/classes/servers-table-data';
import { SidenavService } from 'src/app/services/sidenav.service';

//TODO: add popup to show full server description text in table

@Component({
  selector: 'app-organization-dashboard',
  templateUrl: './organization-dashboard.component.html',
  styleUrls: ['./organization-dashboard.component.scss']
})
export class OrganizationDashboardComponent implements OnInit {

  tableType = TableType;

  displayData: ServersTableData[];
  displayColumns: String[];
  fullscreenColumns: String[];

  constructor(
    public sidenavService: SidenavService,
    public serverService: ServerService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      const orgId = params['organization'];
      
      this.serverService.getServers(orgId)
      .subscribe(serversResponse => {
        const servers = serversResponse.server;

        this.displayColumns = ['name', 'createTime', 'deployed', 'started', 'state'];
        this.fullscreenColumns = ['name', 'description', 'createTime', 'cpuCount', 'memory', 'privateIpv4', 'deployed', 'started', 'state'];
        this.displayData = [];
        servers.forEach(server => {
          this.displayData.push(
            {
              id: server.id,
              name: server.name,
              description: server.description,
              cpuCount: server.cpu.count,
              cpuSpeed: server.cpu.speed,
              cpuCores: server.cpu.coresPerSocket,
              memory: server.memoryGb,
              privateIpv4: server.network.privateIpv4,
              createTime: server.createTime,
              deployed: server.deployed,
              started: server.started,
              state: server.state
            }
          );
        });
      });
    });
  }

}