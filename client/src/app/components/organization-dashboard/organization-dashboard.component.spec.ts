import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationDashboardComponent } from './organization-dashboard.component';
import { TableComponent } from '../table/table.component';
import { SidenavService } from 'src/app/services/sidenav.service';
import { DemoMaterialModule } from 'src/app/material-module';
import { ServerService } from 'src/app/services/servers/server.service';
import { ServerMockService } from 'src/app/services/servers/server-mock.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';

describe('OrganizationDashboardComponent', () => {
  let component: OrganizationDashboardComponent;
  let fixture: ComponentFixture<OrganizationDashboardComponent>;

  const fakeActivatedRoute = {
    params: of({organization: 'orgidtest-122121123'})
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        DemoMaterialModule,
        HttpClientModule
      ],
      declarations: [ 
        OrganizationDashboardComponent,
        TableComponent
      ],
      providers: [
        SidenavService,
        {provide: ServerService, useValue: ServerMockService},
        {provide: ActivatedRoute, useValue: fakeActivatedRoute}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
