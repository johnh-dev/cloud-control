import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { SnackbarService } from '../services/snackbar.service';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {

    constructor(public snackbarService: SnackbarService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log('event: ', event);
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                const data = {
                    reason: error && error.error && error.error.reason ? error.error.reason : error.error.message ? error.error.message : '',
                    status: error.status
                };
                this.snackbarService.show('Error: ' + data.status + '. Message: ' + data.reason);
                return throwError(error);
            }));
    }

}