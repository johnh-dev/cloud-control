import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { GetServersResponse } from '../../classes/get-servers-response';
import { Server } from '../../classes/server';
import { CreateServer } from '../../classes/create-server';
import { CreateServerResponse } from '../../classes/create-server-response';
import { DeleteServerResponse } from '../../classes/delete-server-response';

@Injectable()
export class ServerMockService {

  /**
   * Get servers for organization
   * @param orgId
   * @return {Observable<GetServersResponse>}
   */
  getServers(orgId: string): Observable<GetServersResponse> {
    return of();
  }

  /**
   * Get server details for organization
   * @param orgId
   * @param serverId
   * @return {Observable<Server>}
   */
  getServer(orgId: string, serverId: string): Observable<Server> {
    return of();
  }

  /**
   * Create server for organization
   * @param orgId
   * @param server
   * @return {Observable<CreateServerResponse>}
   */
  createServer(orgId: string, server: CreateServer): Observable<CreateServerResponse> {
    return of();
  }

  /**
   * Edit server for organization
   * @param orgId
   * @param server
   * @return {Observable<CreateServerResponse>}
   */
  editServer(orgId: string, server: Server): Observable<CreateServerResponse> {
    return of();
  }

  /**
   * Delete a server
   * @param orgId
   * @param serverId
   * @return {Observable<DeleteServerResponse>}
   */
  deleteServer(orgId: string, serverId: string): Observable<DeleteServerResponse> {
    return of();
  }

  generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }
}