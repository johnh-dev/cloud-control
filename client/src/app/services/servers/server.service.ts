import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GetServersResponse } from '../../classes/get-servers-response';
import { Server } from '../../classes/server';
import { CreateServer } from '../../classes/create-server';
import { CreateServerResponse } from '../../classes/create-server-response';
import { DeleteServerResponse } from '../../classes/delete-server-response';

@Injectable()
export class ServerService {

  constructor(
    private httpClient: HttpClient
  ) {}

  /**
   * Get servers for organization
   * @param orgId
   * @return {Observable<GetServersResponse>}
   */
  getServers(orgId: string): Observable<GetServersResponse> {
    const url = `http://localhost:3040/api/${orgId}/server`;

    return this.httpClient.get<GetServersResponse>(url)
      .pipe(
        catchError((e) => {
          console.log('Error getting servers', e);
          return throwError(e);
        })
      );
  }

  /**
   * Get server details for organization
   * @param orgId
   * @param serverId
   * @return {Observable<Server>}
   */
  getServer(orgId: string, serverId: string): Observable<Server> {
    const url = `http://localhost:3040/api/${orgId}/server/${serverId}`;

    return this.httpClient.get<Server>(url)
      .pipe(
        catchError((e) => {
          console.log('Error getting server', e);
          return throwError(e);
        })
      );
  }

  /**
   * Create server for organization
   * @param orgId
   * @param server
   * @return {Observable<CreateServerResponse>}
   */
  createServer(orgId: string, server: CreateServer): Observable<CreateServerResponse> {
    const url = `http://localhost:3040/api/${orgId}/server`;
    
    const headers = new  HttpHeaders();
    headers.append('Content-Type', 'application/json');
    const options = { headers: headers };
    return this.httpClient.post<CreateServerResponse>(url, server, options)
    .pipe(
      catchError((e) => {
        console.log('Error creating server', e);
        return throwError(e);
      })
    );
  }

  /**
   * Edit server for organization
   * @param orgId
   * @param server
   * @return {Observable<CreateServerResponse>}
   */
  editServer(orgId: string, server: Server): Observable<CreateServerResponse> {
    const url = `http://localhost:3040/api/${orgId}/server/${server.id}`;
    
    const headers = new  HttpHeaders();
    headers.append('Content-Type', 'application/json');
    const options = { headers: headers };
    return this.httpClient.post<CreateServerResponse>(url, server, options)
    .pipe(
      catchError((e) => {
        console.log('Error editing server', e);
        return throwError(e);
      })
    );
  }

  /**
   * Delete a server
   * @param orgId
   * @param serverId
   * @return {Observable<DeleteServerResponse>}
   */
  deleteServer(orgId: string, serverId: string): Observable<DeleteServerResponse> {
    const url = `http://localhost:3040/api/${orgId}/server/${serverId}`;
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        orgId: orgId,
        serverId: serverId
      }
    };
    return this.httpClient.request<DeleteServerResponse>('delete', url, options)
    .pipe(
      catchError((e) => {
        console.log('Error deleting server', e);
        return throwError(e);
      })
    );
  }

  generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }
}