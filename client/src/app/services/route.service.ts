import { Injectable } from '@angular/core';
import { AppRoute, AppRoutes } from '../classes/routes';
import { Router } from '@angular/router';
import { SidenavService } from './sidenav.service';

/**
 * Provides handling for navigation between app routes (routes defined in RoutesEnum)
 */
@Injectable()
export class RouteService {

  currentRoute: AppRoute;
  currentPath: string;
  pathArgs: string[] = [];

  constructor(
    private router: Router,
    private sidenavService: SidenavService
  ) {}

  setCurrentRoute(currentRoute) {
    this.currentRoute = currentRoute;
  }

  setCurrentPath(currentPath) {
    this.currentPath = currentPath;
  }

  addPathArg(arg: string) {
    this.pathArgs.push(arg);
  }

  popPathArg() {
    this.pathArgs.pop();
  }

  /** 
   *  get last element of this.pathArgs
   *  @return {String}
   *  e.g. 
   *    if the path is '/orgid1/serverid1/create', 
   *    then the args would be ['orgid1', 'serverid1', 'create'],
   *    so the terminal path arg would be 'create'
   */
  getTerminalPathArg() {
    return this.pathArgs[this.pathArgs.length - 1];
  }

  /**
   * Handles change of page route
   * @param {AppRoute} route - route to navigate to
   * @param {string} param - path parameter such as an orgId or serverId e.g. 'sampleOrgId101'
   * @param {number | null | undefined} skipBack - number of args to skip back in pathArgs
   * 
   * TODO: add handling for browser url change scenario (e.g. if specific url path is entered in browser. args would need updating)
   */
  setRoute(route: AppRoute, param?: string, skipBack?: number) {
    if(route === this.currentRoute) {
      // attempt to visit route already selected, just return
      return;
    }

    const lastNode = this.getTerminalPathArg();
    if (skipBack !== undefined && skipBack !== null) {
      for(let i=0; i < skipBack; i++) {
        this.popPathArg();
      }
      this.setCurrentPath(route.getPath(this.pathArgs));
    } else if (param === undefined && !lastNode) {
      switch(route) {
        case AppRoutes.Orgs:
          this.setCurrentPath('');
          break;
        case AppRoutes.Help:
          this.setCurrentPath('help');
          break;
      }
    } else {
      // TODO refactor this block to abstract more
      if(lastNode == 'create' || lastNode == 'edit' || lastNode == 'delete') {
        this.popPathArg();
        if(route === AppRoutes.ServerCreate) {
          this.addPathArg('create');
        } else if (route === AppRoutes.ServerDelete) {
          this.addPathArg('delete');
        } else if (route === AppRoutes.ServerEdit) {
          this.addPathArg('edit');
        }
      } else {
        if(route === AppRoutes.ServerCreate) {
          this.addPathArg('create');
        } else if (route === AppRoutes.ServerDelete) {
          this.addPathArg('delete');
        } else if (route === AppRoutes.ServerEdit) {
          this.addPathArg('edit');
        } else {
          this.addPathArg(param);
        }
      }
      this.setCurrentPath(route.getPath(this.pathArgs));
    }
    this.router.navigateByUrl(this.currentPath);
    this.setCurrentRoute(route);
    this.sidenavService.updateSidenav(this.currentRoute);
  }
}
