import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { GetOrganizationsResponse } from '../../classes/get-organizations-response';

const organizationsMock: GetOrganizationsResponse = require('../../../assets/mocks/organizations.json');

@Injectable()
export class OrganizationsMockService {
  getOrganizations(): Observable<GetOrganizationsResponse>{
    return of(organizationsMock);
  }
}
