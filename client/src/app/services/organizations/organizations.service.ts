import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { GetOrganizationsResponse } from '../../classes/get-organizations-response';

@Injectable()
export class OrganizationsService {

  constructor(
    private httpClient: HttpClient
  ) {}

  /**
   * Get organizations
   * @return {Observable<GetOrganizationsResponse>}
   */
  getOrganizations(): Observable<GetOrganizationsResponse> {
    const url = `http://localhost:3040/api`;

    return this.httpClient.get(url)
      .pipe(
        map((organizations: GetOrganizationsResponse) => organizations),
        catchError((e) => {
          console.log('Error getting organizations', e);
          return throwError(e);
        })
      );
  }
}
