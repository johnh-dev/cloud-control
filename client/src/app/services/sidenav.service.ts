import { Injectable } from '@angular/core';
import { SidenavItem } from '../classes/sidenav-item';
import { AppRoute } from '../classes/routes';
import { ReplaySubject } from 'rxjs';
import { SidenavItems } from '../classes/sidenav-items';

/**
 * Provides handling for the side navigation - opening/closing and setting of sidenav items
 */
@Injectable()
export class SidenavService {

  private sidenavItems: SidenavItem[];
  public isSidenavOpen: boolean;
  public sidenavVisibilityChange: ReplaySubject<boolean> = new ReplaySubject<boolean>();

  constructor()  {
      this.sidenavVisibilityChange.subscribe((value) => {
          this.isSidenavOpen = value;
      });
  }

  /**
   * @returns {SidenavItem[]}
   */
  getSidenavItems(): SidenavItem[] {
    return this.sidenavItems;
  }

  /**
   * sets the active side navigation items for the current route
   * @param route 
   */
  setSidenavItems(route: AppRoute) {
    this.sidenavItems = SidenavItems.getSidenavItems(route);
  }

  /**
   * Sets open/closed visibility state for the side navigation
   * @param opened 
   */
  setSidenavOpen(opened: boolean) {
    this.sidenavVisibilityChange.next(opened);
  }

  /**
   * Updates the side-navigation opened state, and the items that are displayed, by route
   * @param route 
   */
  updateSidenav(route: AppRoute): void {
    this.setSidenavOpen(route.isSidenavOpenByDefault);
    this.setSidenavItems(route);
  }

}
