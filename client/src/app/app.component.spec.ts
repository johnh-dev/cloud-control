import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { OrganizationsComponent } from './components/organizations/organizations.component';
import { OrganizationDashboardComponent } from './components/organization-dashboard/organization-dashboard.component';
import { ServerCreateComponent } from './components/server-create/server-create.component';
import { ServerDashboardComponent } from './components/server-dashboard/server-dashboard.component';
import { ServerDeleteComponent, ServerDeleteConfirmDialog } from './components/server-delete/server-delete.component';
import { ServerEditComponent } from './components/server-edit/server-edit.component';
import { TableComponent } from './components/table/table.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { HelpComponent } from './components/help/help.component';
import { StepperComponent } from './components/stepper/stepper.component';
import { ProgressSpinnerComponent } from './components/progress-spinner/progress-spinner.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialModule } from './material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OrganizationsService } from './services/organizations/organizations.service';
import { SidenavService } from './services/sidenav.service';
import { RouteService } from './services/route.service';
import { ServerService } from './services/servers/server.service';
import { ProgressSpinnerService } from './services/progress-spinner.service';
import { ProgressSpinnerInterceptor } from './interceptors/progress-spinner.interceptor';
import { ErrorHandlerInterceptor } from './interceptors/error-handler.interceptor';
import { APP_BASE_HREF } from '@angular/common';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        DemoMaterialModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [
        AppComponent,
        OrganizationsComponent,
        OrganizationDashboardComponent,
        ServerCreateComponent,
        ServerDashboardComponent,
        ServerDeleteComponent,
        ServerEditComponent,
        TableComponent,
        NavigationComponent,
        HelpComponent,
        StepperComponent,
        ProgressSpinnerComponent,
        ServerDeleteConfirmDialog
      ],
      providers: [
        OrganizationsService,
        SidenavService,
        RouteService,
        ServerService,
        ProgressSpinnerService,
        { provide: HTTP_INTERCEPTORS, useClass: ProgressSpinnerInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorHandlerInterceptor, multi: true },
        { provide: APP_BASE_HREF, useValue : '/' }
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

});
