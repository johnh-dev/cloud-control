import { AppRoutes, AppRoute } from './routes';
import { SidenavItem } from './sidenav-item';

/**
 * Stores side navigation configurations per route
 */
export class SidenavItems {

    static help = [
        new SidenavItem(
          'Back', 
          AppRoutes.Orgs, 
          true, 
          'back-button', 
          null, 
          1,
          null,
          null,
          null,
          'keyboard_backspace'
        ),
    ];
    
    static organizations = [
        new SidenavItem(
          'Help', 
          AppRoutes.Help, 
          true, 
          'help-button', 
          null,
          null,
          'Navigation to a useful help page', 
          'Some useful information on this page', 
          'below',
          'help_outline'
        )
    ];
    
    static organizationDashboard = [
        new SidenavItem('Back', AppRoutes.Orgs, true, 'back', null, 1, null, null, null, 'keyboard_backspace'),
        new SidenavItem('All Servers', AppRoutes.OrgDashboard, true, 'all-servers', null, null, null, null, null, 'view_headline'),
        new SidenavItem('Create', AppRoutes.ServerCreate, true, 'create-server', null, null, null, null, null, 'create')
    ];
    
    static serverDashboard = [
        new SidenavItem('Back', AppRoutes.OrgDashboard, true, 'back', null, 1, null, null, null, 'keyboard_backspace'),
        new SidenavItem('View', AppRoutes.ServerDashboard, true, 'view-servers', null, null, null, null, null, 'pageview'),
        new SidenavItem('Edit', AppRoutes.ServerEdit, true, 'edit-server', null, null, null, null, null, 'edit'),
        new SidenavItem('Delete', AppRoutes.ServerDelete, true, 'delete-server', null, null, null, null, null, 'delete')
    ];

    static getSidenavItems(route: AppRoute): SidenavItem[] {
        switch (route) {
            case AppRoutes.Help:
              return SidenavItems.help;
            case AppRoutes.Orgs:
              return SidenavItems.organizations;
            case AppRoutes.OrgDashboard:
            case AppRoutes.ServerCreate:
              return SidenavItems.organizationDashboard;
            case AppRoutes.ServerDashboard:
            case AppRoutes.ServerDelete:
            case AppRoutes.ServerEdit:
              return SidenavItems.serverDashboard;
          }
    }
}