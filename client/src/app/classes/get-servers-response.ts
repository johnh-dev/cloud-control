import { Server } from './server';

export interface GetServersResponse {
    organizationId: string;
    organizationName: string;
    totalServerCount: number;
    server: Server[];
}