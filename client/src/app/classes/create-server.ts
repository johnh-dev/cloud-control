
export interface CreateServer {
    id: string;
    name: string;
    description: string;
    cpu: {
        count: number;
        speed: string;
        coresPerSocket: number;
    };
    memoryGb: number;
    network: {
        id: string;
        privateIpv4: string;
        vlanId: string;
        vlanName: string;
    };
    createTime: Date;
    deployed: boolean;
    started: boolean;
    state: string;
}