
export interface CreateServerDTO {
    name: string,
    description: string,
    cpu: {
        count: number,
        speed: string,
        coresPerSocket: number
    },
    memoryGb: number,
    network: {
        privateIpv4: string,
        vlanName: string
    },
    deployed: boolean,
    started: boolean
};