import { Organization } from './organization';

export interface GetOrganizationsResponse {
    organizations: Organization[];
}