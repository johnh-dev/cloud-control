
enum RouteNames {
    help = 'Help',
    orgs = 'Orgs',
    orgDashboard = 'OrgDashboard',
    serverCreate = 'ServerCreate',
    serverDashboard = 'ServerDashboard',
    serverDelete = 'ServerDelete',
    serverEdit = 'ServerEdit'
}
class SidenavDefaultVisibilityStates {
    static help = true;
    static orgs = false;
    static orgDashboard = true;
    static serverCreate = true;
    static serverDashboard = true;
    static serverDelete = true;
    static serverEdit = true;
  }
class RouteEndpoints {
    // reusable placeholders
    static organization = ':organization';
    static server = ':server';

    // routes
    static help : string = 'help';
    static orgs : string = '';
    static orgDashboard : string = RouteEndpoints.organization;
    static serverCreate : string = RouteEndpoints.organization + '/create';
    static serverDashboard : string = RouteEndpoints.organization + '/' + RouteEndpoints.server;
    static serverDelete : string = RouteEndpoints.organization + '/' + RouteEndpoints.server + '/delete';
    static serverEdit : string = RouteEndpoints.organization + '/' + RouteEndpoints.server + '/edit';
}

abstract class AbstractRoute {
    // reusable placeholders
    static organization = ':organization';
    static server = ':server';

    constructor(
        public name: string,
        public path: string,
        public isSidenavOpenByDefault: boolean
    ){}

    /**
     * 
     * @param params array of string parameters e.g. ['sampleOrgId1','sampleServerId101'] 
     * @returns {string} path for this route, with placeholders replaced with params
     */
    getPath(params: string[]): string {
        const placeholderCount = this.path.split(':').length - 1;
        if(placeholderCount === -1) {
            return this.path;
        } else {
            if(placeholderCount == 1) {
                return this.path.replace(AbstractRoute.organization, params[0]);
            } else if(placeholderCount == 2) {
                return this.path.replace(AbstractRoute.organization, params[0]).replace(AbstractRoute.server, params[1]);
            }
        }
    }
}
export class AppRoute extends AbstractRoute{
    constructor(
        public name: string,
        public path: string,
        public isSidenavOpenByDefault: boolean
    ){
        super(name, path, isSidenavOpenByDefault);
    }
}

export class AppRoutes {

    static Help : AppRoute = new AppRoute(
        RouteNames.help,
        RouteEndpoints.help,
        SidenavDefaultVisibilityStates.help
    )

    static Orgs : AppRoute = new AppRoute(
        RouteNames.orgs,
        RouteEndpoints.orgs,
        SidenavDefaultVisibilityStates.orgs
    );
    
    static OrgDashboard : AppRoute = new AppRoute(
        RouteNames.orgDashboard,
        RouteEndpoints.organization,
        SidenavDefaultVisibilityStates.orgDashboard
    );

    static ServerCreate : AppRoute = new AppRoute(
        RouteNames.serverCreate,
        RouteEndpoints.serverCreate,
        SidenavDefaultVisibilityStates.serverCreate
    );

    static ServerDashboard : AppRoute = new AppRoute(
        RouteNames.serverDashboard,
        RouteEndpoints.serverDashboard,
        SidenavDefaultVisibilityStates.serverDashboard
    );

    static ServerDelete : AppRoute = new AppRoute(
        RouteNames.serverDelete,
        RouteEndpoints.serverDelete,
        SidenavDefaultVisibilityStates.serverDelete
    );

    static ServerEdit : AppRoute = new AppRoute(
        RouteNames.serverEdit,
        RouteEndpoints.serverEdit,
        SidenavDefaultVisibilityStates.serverEdit
    );
}