import {Server} from './server';

export interface Organization {
    id: string;
    name: string;
    server: Server[];
}