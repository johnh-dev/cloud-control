export interface ServersTableData {
    id: string;
    name: string;
    description: string;
    cpuCount: number;
    cpuSpeed: string;
    cpuCores: number;
    memory: number;
    privateIpv4: string;
    createTime: Date;
    deployed: boolean;
    started: boolean;
    state: string;
}