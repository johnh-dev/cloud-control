export interface OrganizationsTableData {
    id: string;
    name: string;
    serverCount: number;
}