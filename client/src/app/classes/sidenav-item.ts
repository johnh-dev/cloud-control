import { AppRoute } from './routes';


export class SidenavItem {
    constructor(
        public text: string,
        public route?: AppRoute,
        public isButton?: boolean,
        public cssClass?: string,
        public func?: Function,
        public skipBack?: number,
        public ariaLabel?: string,
        public tooltip?: string,
        public tooltipPosition?: string,
        public matIcon?: string
    ) {}

    execute() {
        this.func();
    }
}